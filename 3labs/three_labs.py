import numpy as np
import bitarray as ba

class CyclicCode:
    def __init__(self, k, n, g, t):
        self.k = k
        self.n = n
        self.g = g
        self.t = t

    def get_t(self):
        return self.t

    def get_k(self):
        return self.k

    def get_n(self):
        return self.n

    def get_g(self):
        return self.g

    def encode(self, a):
        c = [0] * self.get_n()
        c[self.get_n() - self.get_k():self.get_n()] = a
        r = self.remainder(c)
        c[0:self.get_n() - self.get_k()] = r
        return c

    def xor(self, a, b):
        return [1 if a[j] != b[j] else 0 for j in range(self.get_n())]

    def remainder(self, c):
        r = np.full(self.get_n(), 0)
        for i, j in enumerate(c):
            r[i] = j
        i = self.get_n() - 1
        while i >= self.get_n() - self.get_k():
            if r[i]:
                r[i - (self.get_n() - self.get_k()):i + 1] = r[i - (self.get_n() - self.get_k()):i + 1] ^ self.get_g()
            i -= 1
        return r[0:len(self.get_g()) - 1]

    def EncodySys(self, a):
        c = [0, 0, 0, 0, 0, 0, 0]
        vec = [0, 0, 0, 0, 0, 0, 0]
        c[self.get_n() - self.get_k(): self.get_n()] = a
        r = self.remainder(c)
        vec[self.get_n() - self.get_k(): self.get_n()] = a
        vec[0:self.get_n() - self.get_k()] = r[0:self.get_n() - self.get_k()]
        return vec

    def HammingWeight(self, val):
        res = [int(i) for i in val if int(i) == 1]
        return len(res)

    def MakeTable(self):
        syndromes = {}
        for i in range(2**self.get_n()):
            err = [bin(i)[2:].zfill(self.get_n())]
            error = [0] * self.get_n()
            for j, item in enumerate(err[0]):
                error[j] = int(item)
            if self.HammingWeight(error) <= self.get_t():
                key = self.remainder(error)
                syndromes[tuple(key)] = error
        return syndromes

    def decoder(self, file, g):
        # reading message
        message = ba.bitarray()
        with open(file, 'rb') as f:
            message.fromfile(f)

        ar = list(message)
        if (len(ar) % self.get_k()) != 0:
            A = np.zeros((len(ar) // self.get_k() + 1)*self.get_k())
        else:
            A = np.zeros(len(ar))
        for k in range(0, len(ar)):
            if ar[k]:
                A[k] = 1
            if not ar[k]:
                A[k] = 0

        # coding message
        coded = []
        i = 0
        for j in range(0, len(A), self.get_k()):
            b = []
            b[j:j+self.get_k()] = A[j:j+self.get_k()]
            coded[i:i + self.get_n()] = self.encode(b)
            i = i + self.get_n()

        # decoding message
        i = 0
        decoded = []
        coded[19] = 1
        for j in range(0, len(coded), self.get_n()):
            string = coded[j:j + self.get_n()]
            remainder = self.remainder(string)
            if np.count_nonzero(remainder) == 0:
                decoded[i:i + self.get_k()] = string[self.get_k() + 1:self.get_n()]
            else:
                temp = self.xor(string, [int(n) for n in self.MakeTable()[tuple(remainder)]])
                decoded[i:i + self.get_k()] = temp[self.get_k() + 1:self.get_n()]
            i += self.get_k()
        res = decoded[0:len(decoded) - (len(decoded) - len(ar))]

        # output decoded message
        out = ba.bitarray()
        out.extend(res)
        with open('decoded-message.txt', 'wb') as f:
            out.tofile(f)

def tasks5(k, n, g, t):
    # task 1
    task1 = CyclicCode(k, n, g, t)
    print('task 1: \n  k = ' + str(task1.get_k()) + '\n  n = ' + str(task1.get_n()) + '\n  g = ' + str(
        task1.get_g()) + '\n  t = ' + str(task1.get_t()))

    # task 5
    task5 = task1.MakeTable()
    print('task 5:' + ' \n  Table syndromes = ')
    for key, code in task5.items():
        print("  {0}: {1}".format(code, key))

def tasks6(k, n, g, t):
    # task 1
    task1 = CyclicCode(k, n, g, t)
    print('task 1: \n  k = ' + str(task1.get_k()) + '\n  n = ' + str(task1.get_n()) + '\n  g = ' + str(
        task1.get_g()) + '\n  t = ' + str(task1.get_t()))

    # task 6
    file = "message.txt"
    print('task 6: \n  name file with message ' + file)
    task1.decoder(file, g)

def main():
    k = 4
    n = 7
    g = [1, 0, 1, 1]
    t = 1

    tasks5(k, n, g, t)

    # update input data
    k = 7
    n = 15
    g = [1, 0, 0, 0, 1, 0, 1, 1, 1]
    t = 2

    tasks6(k, n, g, t)

if __name__ == "__main__":
    main()