from numpy.polynomial import Polynomial as P

class CyclicCode:
    def __init__(self, k, n, g):
        self.k = k
        self.n = n
        self.g = g

    def get_k(self):
        return self.k

    def get_n(self):
        return self.n

    def get_g(self):
        return self.g

    def encode(self, a):
        c = [0, 0, 0, 0, 0, 0, 0]
        for i, i1 in enumerate(a):
            for j, j1 in enumerate(self.get_g()):
                c[i + j] += i1 * j1
        for i, j in enumerate(c):
            c[i] = j % 2
        return c

    def remainder(self, c):
        r = P(c) % P(self.get_g())
        R = r.coef % 2
        return R

    def EncodySys(self, a):
        c = [0, 0, 0, 0, 0, 0, 0]
        vec = [0, 0, 0, 0, 0, 0, 0]
        c[self.get_n() - self.get_k(): self.get_n()] = a
        r = self.remainder(c)
        vec[self.get_n() - self.get_k(): self.get_n()] = a
        vec[0:self.get_n() - self.get_k()] = r[0:self.get_n() - self.get_k()]
        return vec

def _main():
    # task 1
    task1 = CyclicCode(4, 7, [1, 0, 1, 1])
    print('task 1: \n k = ' + str(task1.get_k()) + '\n n = ' + str(task1.get_n()) + '\n g = ' + str(task1.get_g()))

    # task 2
    a = [0, 1, 1, 0]
    task2 = task1.encode(a)
    print('task 2: \n a = ' + str(a) + '\n c = ' + str(task2))

    # task 3
    c = [1, 1, 1, 1, 0, 1, 0]
    task3 = task1.remainder(c)
    print('task 3: \n c = ' + str(c) + '\n r = ' + str(task3))

    # task 4
    task4 = task1.EncodySys(a)
    print('task 4: \n a = ' + str(a) + ' \n c = ' + str(task4))

if __name__ == '__main__':
    _main()
