import numpy as np

class GolayCode:
    def __init__(self, k, n, B, G, H):
        self.k = k
        self.n = n
        self.B = B
        self.G = G
        self.H = H

    def get_k(self):
        return self.k

    def get_n(self):
        return self.n

    def get_B(self):
        return self.B

    def get_G(self):
        return self.G

    def get_H(self):
        return self.H

    def encode(self, a):
        w = a.dot(self.G) % 2
        return w[0]

    def HammingWeight(self, val):
        res = [int(i) for i in val if int(i) == 1]
        return len(res)

    def get_message(self, w, u):
        message = w
        for i in range(24):
            if u[i] == 1:
                message[i] = 1 - w[i]
        return message

    def decode(self, w):
        u = np.zeros(24)
        w = w[0]
        wh = w.dot(self.H) % 2
        wt = self.HammingWeight(wh)
        if wt <= 3:
            u[0:12] = wh
            return self.get_message(w, u)
        else:
            for j in range(wt):
                temp = np.zeros(12)
                for i in range(12):
                    if ((wh[i] == 1) & (self.B[j][i] == 1)) | ((wh[i] == 0) & (self.B[j][i] == 0)):
                        temp[i] = 0
                    else:
                        temp[i] = 1
                wt = self.HammingWeight(temp)

                if wt <= 2:
                    u[0:12] = temp
                    u[12:24] = self.G[j][0:12]
                    break
            if wt > 2:
                sB = wh.dot(self.B) % 2
                wtB = self.HammingWeight(sB)
                if wtB <= 3:
                    u[12:24] = sB
                    return self.get_message(w, u)
                else:
                    for j in range(wtB):
                        temp = np.zeros(12)
                        for i in range(12):
                            if ((sB[i] == 1) & (self.B[j][i] == 1)) | ((sB[i] == 0) & (self.B[j][i] == 0)):
                                temp[i] = 0
                            else:
                                temp[i] = 1
                        wtB = self.HammingWeight(temp)

                        if wtB <= 2:
                            u[0:12] = self.G[j][0:12]
                            u[12:24] = temp
                            break
                        else:
                            print("Sorry, the error cannot be corrected!")
            return self.get_message(w, u)

def _main():
    # task 1
    k = 12
    n = 24
    B = np.array([[1, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 1],
                  [1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 1, 1],
                  [0, 1, 1, 1, 0, 0, 0, 1, 0, 1, 1, 1],
                  [1, 1, 1, 0, 0, 0, 1, 0, 1, 1, 0, 1],
                  [1, 1, 0, 0, 0, 1, 0, 1, 1, 0, 1, 1],
                  [1, 0, 0, 0, 1, 0, 1, 1, 0, 1, 1, 1],
                  [0, 0, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1],
                  [0, 0, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1],
                  [0, 1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 1],
                  [1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 1],
                  [0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 1, 1],
                  [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
                  ])
    I = np.eye(12)
    G = np.concatenate((I, B), axis=1)
    H = np.concatenate((I, B))

    task1 = GolayCode(k, n, B, G, H)
    print('task 1: \n k = ' + str(task1.get_k()) + '\n n = ' + str(task1.get_n()) + '\n B = ' + str(task1.get_B()))

    # task 2
    a = np.array([0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1]).reshape(1, 12)
    task2 = task1.encode(a)
    print('task 2:' + '\n G = ' + str(task1.get_G()) + ' \n a = ' + str(a) + ' \n w = ' + str(task2))

    # task 3
    #w = task2
    w = np.array([0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0]).reshape(1, 24)
    task3 = task1.decode(w)
    print('task 3: \n' + ' w = ' + str(w) + '\n message = ' + str(task3))

if __name__ == '__main__':
    _main()
