import numpy as np
import bitarray as ba

class RMCode:
    def __init__(self, r, m, k, n):
        self.r = r
        self.m = m
        self.k = k
        self.n = n

    def get_r(self):
        return self.r

    def get_m(self):
        return self.m

    def get_k(self):
        return self.k

    def get_n(self):
        return self.n

    def GG(self, r, m):
        if r == 0:
            return np.ones((1, 2 ** m))
        if r == m:
            last = np.zeros((1, 2 ** m))
            last[0][-1] = 1
            return np.concatenate([self.GG(m - 1, m), last])

        return np.concatenate([np.concatenate([self.GG(r, m - 1), self.GG(r, m - 1)], axis=1),
                               np.concatenate([np.zeros((1, 2 ** (m - 1))), self.GG(r - 1, m - 1)], axis=1)])

    def H(self, i, m):
        H = np.array([[1, 1], [1, -1]])
        return np.kron(np.kron(np.eye(2 ** (m - i)), H), np.eye(2 ** (i - 1)))

    def encoder(self, a, G):
        w = a.dot(G) % 2
        return w[0]

    def decoder(self, w):
        w_w = np.ones((len(w)))
        for i in range(len(w)):
            if w[i] == 0:
                w_w[i] = -1
        abs = np.zeros(self.get_m())
        W = []
        print(w_w)
        for k in range(self.get_m()):
            H_im = self.H(k + 1, self.get_m())
            W.append(w_w.dot(H_im))
            abs[k] = np.argmax(np.abs(W))
        j = int(max(abs))
        ind = int(np.argmax(abs))
        vj = [0, 0, 0, 0]
        v = bin(j)
        vj[1:len(v)-1] = v[2:len(v)+1]
        if W[ind][j] > 0:
            vj[0] = 1
            return vj
        else:
            return vj




def K(r, m):
    K = 0
    for i in range(0, r + 1):
        K += np.math.factorial(m + i - 1) / (np.math.factorial(i) * np.math.factorial(m - 1))
    return K

def _main():
    # task 1
    r = 2
    m = 4
    k = int(K(r, m))
    n = 2 ** m

    task1 = RMCode(r, m, k, n)
    print('task 1: \n r = ' + str(task1.get_r()) + '\n m = ' + str(task1.get_m()) + '\n k = ' + str(task1.get_k()) + '\n n = ' + str(task1.get_n()))

    # task 2
    a = np.array([1, 1, 1, 0]).reshape(1, 4)
    task2_1 = task1.GG(r, m)
    print('task 2:' + '\n a = ' + str(a[0]) + '\n G = ' + str(task2_1))
    task2_2 = task1.encoder(a, task2_1)
    print('\n w = ' + str(task2_2))
    task2_3 = task1.decoder(task2_2)
    print('\n m = ' + str(task2_3))


if __name__ == '__main__':
    _main()
