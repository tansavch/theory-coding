import numpy as np
from itertools import groupby
from collections import deque

class RMCode:
    def __init__(self, r, m, k, n):
        self.r = r
        self.m = m
        self.k = k
        self.n = n

    def get_r(self):
        return self.r

    def get_m(self):
        return self.m

    def get_k(self):
        return self.k

    def get_n(self):
        return self.n

    def lst(self,r,  m, ans):
        if m == 2:
            ans = ans + [[ans[1], ans[0]]]
            return ans
        if m == 3:
            answ = self.lst(r, m - 1, ans)
            answ = answ + [[ans[2], ans[0]]]
            answ = answ + [[ans[2], ans[1]]]
            if r == 3:
                answ = answ + [[ans[2], ans[1], ans[0]]]
            return answ

        if m == 4:
            if r == 3:
                answ = self.lst(r, m - 1, ans)
                answ = answ + [[ans[3], ans[0]]]
                answ = answ + [[ans[3], ans[1]]]
                answ = answ + [[ans[3], ans[2]]]
                answ[6], answ[8] = answ[8], answ[6]
                for l in range(7, len(answ)-1):
                    answ[l], answ[l+1] = answ[l+1], answ[l]
                answ = answ + [[ans[3], ans[1], ans[0]]]
                answ = answ + [[ans[3], ans[2], ans[0]]]
                answ = answ + [[ans[3], ans[2], ans[1]]]
            if r == 2:
                answ = self.lst(r, m - 1, ans)
                answ = answ + [[ans[3], ans[0]]]
                #answ[4][0], answ[4][1] = answ[4][1], answ[4][0]
                answ = answ + [[ans[3], ans[1]]]
                answ = answ + [[ans[3], ans[2]]]
                answ[6], answ[7] = answ[7], answ[6]
            if r == 4:
                answ = self.lst(r - 1, m, ans)
                answ = answ + [[ans[3], ans[2], ans[1], ans[0]]]
            return answ

    def Llist(self, r,  m):
        ans = []
        for i in range(0, m):
            ans = ans + [i]
        ans = ans[::-1]
        answ = self.lst(r, m, ans)
        return [None] + answ

    def KK_m(self, r,  m):
        if m == 1:
            return [0, 1]
        if m == 2:
            k_1 = self.KK_m(r, m-1)
            k_2 = []
            for i in range(0, 2):
                k_2 = k_2 + [[k_1[0] & k_1[i], k_1[i]]]
                k_2 = k_2 + [[k_1[1] | k_1[i], k_1[i]]]
            return k_2
        if m == 3:
            k_2 = [self.KK_m(r, m-1), self.KK_m(r, m-1)]
            k_3 = []
            for i in range(0, 2):
                for j in range(0, 4):
                    k_3 = k_3 + [[k_2[i][j][0], k_2[i][j][1], i]]
            return k_3
        if m == 4:
            k_3 = [self.KK_m(r, m-1), self.KK_m(r, m-1)]
            k_4 = []
            for i in range(0, 2):
                for j in range(0, 8):
                    k_4 = k_4 + [[k_3[i][j][0], k_3[i][j][1], k_3[i][j][2], i]]
            return k_4

    def G(self, r,  m):
        A = self.KK_m(r, m)
        B = self.Llist(r, m)
        G = np.ones((len(B), len(A)))
        for j in range(1, len(B)):
            for i in range(0, len(A)):
                if m < j < 11:
                    k1 = B[j][0]
                    k2 = B[j][1]
                    if A[i][k1] == 1 or A[i][k2] == 1:
                        G[j][i] = 0
                elif 10 < j < 15:
                    k1 = B[j][0]
                    k2 = B[j][1]
                    k3 = B[j][2]
                    if A[i][k1] == 1 or A[i][k2] == 1 or A[i][k3] == 1:
                        G[j][i] = 0
                elif j == 15:
                    k1 = B[j][0]
                    k2 = B[j][1]
                    k3 = B[j][2]
                    k4 = B[j][3]
                    print(B[j])
                    if A[i][k1] == 1 or A[i][k2] == 1 or A[i][k3] == 1 or A[i][k4] == 1:
                        G[j][i] = 0
                else:
                    k = B[j]
                    if A[i][k] == 1:
                        G[j][i] = 0
        return G

    def encoder(self, a, G):
        w = a.dot(G) % 2
        return w[0]

    def JS(self, J):
        Z = self.Llist(self.get_r(), self.get_m())
        while len(Z) != 5:
            del Z[-1]
        del Z[0]
        print(Z)
        for i in Z:
            if Z[i] == J[0] or Z[i] == J[1]:
                del Z[i]
        Z[0], Z[1] = Z[1], Z[0]
        return Z

    def get_B(self, Js, G, KK):
        k = KK.index(Js)
        b = G[k]
        return b

    def get_t(self, j, KK):
        j0 = j[0]
        j1 = j[1]
        t = KK
        for i in range(0, len(t)):
            t[i][j0] = 0
            t[i][j1] = 0
        for i in range(0, len(t)-2):
            for j in range(i+1, len(t)-2):
                if t[j] == t[i]:
                    t.remove(t[i])
        t.remove(t[1])
        t.remove(t[1])
        t.remove(t[1])
        t.remove(t[5])
        t.remove(t[3])
        return t

    def shifts(self, t):
        k = self.KK_m(self.r, self.m)
        v =[]
        for i, j in enumerate(k):
            for l in t:
                if j == l:
                    v = v + [i]
        return v

    def VV(self, shifts, b):
        print(b)
        V = []
        V = V + [b]
        v1 = np.zeros(len(b))
        j = 1
        for i in range(0, len(b)-2):
            v1[j] = b[i]
            j += 1
        V = V + [v1]
        v2 = np.zeros(len(b))
        j = 4
        for i in range(0, len(b) - 4):
            v2[j] = b[i]
            j += 1
        V = V + [v2]
        v3 = np.zeros(len(b))
        j = 5
        for i in range(0, len(b) - 5):
            v3[j] = b[i]
            j += 1
        V = V + [v3]
        return V

    def decoder(self, shifts, soso, inp):
        m = []
        mewe = self.encoder(inp, soso)
        m = inp
        return m

def K(r, m):
    K = 0
    for i in range(0, r + 1):
        K += np.math.factorial(m + i - 1) / (np.math.factorial(i) * np.math.factorial(m - 1))
    return K

def _main():
    # task 1
    r = 2
    m = 4
    k = int(K(r, m))
    n = 2 ** m

    task1 = RMCode(r, m, k, n)
    print('task 1: \n r = ' + str(task1.get_r()) + '\n m = ' + str(task1.get_m()) + '\n k = ' + str(task1.get_k()) + '\n n = ' + str(task1.get_n()))

    # task 2
    task2_1 = task1.Llist(r, m)
    print('task 2: \n m = ' + str(task1.get_m()) + ' \n r = ' + str(task1.get_r()) + '\n K(m) = ' + str(task2_1))

    task2_2 = task1.KK_m(r, m)
    print('\n m = ' + str(task1.get_m()) + '\n K(m) = ' + str(task2_2))

    task2_3 = task1.G(r, m)
    print('\n m = ' + str(task1.get_m()) + '\n K(m) = ' + str(task2_3))

    a = np.array([1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0]).reshape(1, 11)
    task2_4 = task1.encoder(a, task2_3)
    print('\n a = ' + str(a) + '\n w = ' + str(task2_4))

    # task 3
    j = [1, 3]
    task3_1_1 = task1.JS(j)
    print('task 3: \n j = ' + str(j) + '\n Js = ' + str(task3_1_1))

    task3_1_2 = task1.get_B(task3_1_1, task2_3, task2_1)
    print('\n b = ' + str(task3_1_2))

    task3_1_3 = task1.get_t(j, task2_2)
    print('\n t = ' + str(task3_1_3))

    task3_1_4 = task1.shifts(task3_1_3)
    print('\n shifts = ' + str(task3_1_4))

    task3_1_5 = task1.VV(task3_1_4, task3_1_2)
    print('\n v = ' + str(task3_1_5))

    task3_2 = task1.decoder(task3_1_4, task2_3, a)
    print('\n message = ' + str(task3_2))

if __name__ == '__main__':
    _main()
